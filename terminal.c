/*
// COSC 439 Operating Systems.
// Final Project.
// YOUR NAME: Olaniyan Eni, Idowu Adeseun, Olencz Kevin.
// PROGRAM-NAME: LINUX SHELL.
*/


#include "commands.h"
#include "declare.h"

// make sure you check the readme after pulling

// TODO:
// () Make a separate window appear when running this after compiling: should be able to double click to run the compiled file
// () A switch statement in run() that checks the command and runs it in its own process (fork()) if it exists, throw error otherwise
// () More commands!

int main(int argc, char *argv[])
{
	// init values
	command.lineLength = 1000;
	command.argLength = 100;
	//initArray(command);
    ////////////////////// (1) implement opening the program in its own window
    int result = terminalLoop();
    if (result != 0)
        printf( RED "Loop crashed with error code: %d" NORMAL, result);
}

int terminalLoop()
{
    bool running = true;

    printWelcome();
    printDirectory();

    while (running)
    {
    	// reset number of arguments given
	    command.numArgs = 0;
	    // check for commands/user input
		fgets(command.line,1000,stdin);
		// remove newline character
		command.line[strcspn(command.line, "\n")] = '\0';
		// parse the line into commands and then attempt to run them
		tokenize(&command);
		int status = run(command);
		if (status != 0)
			return status;
	    // return to loop and display directory or process exit commmand
		printDirectory();
    }

    return -1;
}



int run(struct Command command)
{
	// the first token should be the command, followed by the arguments
	// these print functions are for testing use
	//printf( YELLOW "command typed is: " NORMAL);
	//printf("%s\n", command.args[0]);
	//printf( YELLOW "the argument length is: " NORMAL);
	//printf("%d\n", command.numArgs);

	// handle commands
	// textEqual returns true if two strings match
	// lsText, cdText, mkDirText ... all have several options for input
	// return true if the input matches any allowed command "wordings"
	if (quitText(command.args[0]))
		quit();
	else if (textEqual(command.args[0], "help"))
		help(command);
	else if (lsText(command.args[0]))
		return ls(command);
	else if (moveText(command.args[0]))
		 move(command);
	else if (rnameText(command.args[0]))
		 rname(command);
	else if(cdText(command.args[0]))
		 return changeDir(command);
	else if (mkdirText(command.args[0]))
		 return makeDir(command);
	else if(textEqual(command.args[0], "makeFile"))
		 return makeFile(command);
	else if (runText(command.args[0]))
		return runExternal(command);
	else if (rmText(command.args[0]))
		return rmv(command);
	else
	{
		printf("The command: %s, does not exist.\n", command.args[0]);
	}
	 return 0;
}








// breaks up the given input into an array of arguments
void tokenize(struct Command* cmd)
{
	// commands will be the tokenized array version of line
	char* commands[100];
	char spaces[] = " ";
     // break the line up into tokens
     char *ptr = strtok(cmd->line, spaces);
     // loop unitl you have split the line into tokens
	 for (int i = 0; ptr != NULL; i++)
	 {
		cmd->args[i] = ptr;
		cmd->numArgs += 1;
		ptr = strtok(NULL, spaces);
	 }
}

void printWelcome()
{
    printf(BLUE "Welcome to Simple Linux Shell!\nType \'help\' for command help\n" NORMAL);
}



// not currently in use
void initArray(struct Command command)
{
	for (int i = 0; i < command.argLength; i++)
	{
		command.args[i] = "$null$";
	}
}
