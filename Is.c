/*
// COSC 439 Operating Systems.
// Final Project.
// YOUR NAME: Olaniyan Eni, Idowu Adeseun, Olencz Kevin.
// PROGRAM-NAME: LINUX SHELL.
*/

#include "commands.h"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"


// returns true if two strings are "equal"
bool textEqual(char text[], char text2[])
{
	if (strcmp(text, text2) == 0)
		return true;
	else
		return false;
}

// for use in quick sort when arg sort is passed
/* qsort C-string comparison function */
// http://www.anyexample.com/programming/c/qsort__sorting_array_of_strings__integers_and_structs.xml
int compare(const void *a, const void *b) 
{ 
    const char **ia = (const char **)a;
    const char **ib = (const char **)b;
    return strcmp(*ia, *ib);
	/* strcmp functions works exactly as expected from
	comparison function */ 
} 


// ls function displays all files and directories in the given directory
// ls makes calls using dirent.h
// dirent.h includes methods to open/close/traverse directory streams

int ls(struct Command cmd)
{
	// A structure that holds basic info about a directory, including the name (import for ls)
	struct dirent *d;
	// this is a pointer to a directory object
	DIR *dirP;
	// To hold the current directory to view
	char* dir;
	// if no arguments are given, use current directory
	char current[] = ".";
	dir = current;
	// to count how many objects are in the directory and ptr to a dynamically allocated array
	// this is used if sort is passed as an argument
	int counter = 0;
	char** aPtr;
	// if arguments are given, use second argument as path
	if (cmd.numArgs > 1)
	{
		dir = cmd.args[1];
	}
	// opens a directory "stream" to the directory passed by the argument
	// if successful, returns a pointer to a directory object
	dirP = opendir(dir);
	// if the directory was not successfully "opened"
	if (dirP == NULL)
	{
		printf(RED "An error occured trying to read this directory\n" NORMAL);
		if (cmd.numArgs > 1)
			printf(YELLOW "Please make sure the directory: %s exists\n" NORMAL, cmd.args[1]);
		return 0;
	}
	else if (cmd.numArgs > 2) // handle more args
	{
		if (textEqual(cmd.args[2], "sort"))
		{
			// count number of objects in the file
			while ((d = readdir(dirP)))
				counter++;
			// rewind the stream iterator to the beginning
			rewinddir(dirP);
			// dynamically allocate string array
			aPtr = malloc(counter * sizeof(char*));
			for (int i = 0; i < counter; i++)
				aPtr[i] = (char*)malloc(200*sizeof(char));
		}
	}
	// this while loop will traverse the stream of objects in the directory
	// d will point to the next structure in the stream everytime readdir(directory*) is called
	// cnt is used for the sort arg for index assignment
	int cnt = 0; 
	while((d = readdir(dirP)))
	{
		// access the current structure and print its name member
		// d_name is the name
		// d_type is the type of file (not supported on all platforms):
		// type 4 is a directory and type 8 is file
		// (you can find this by printing d_type)
		if (cmd.numArgs <= 2 || (cmd.numArgs > 2 && !textEqual(cmd.args[2],"sort")))
		{
			if (d->d_type > 5)
				printf("%s\t",d->d_name);
			else
				printf(YELLOW "%s\t" NORMAL,d->d_name);
		} // if we need to sort the array then we have to store the files for sorting
		else if (cmd.numArgs > 2 && textEqual(cmd.args[2], "sort"))
		{
			aPtr[cnt] = d->d_name;
			cnt++;
		}
	}
	// sort and then print the files if sort arg is passed
	if (cmd.numArgs > 2 && textEqual(cmd.args[2], "sort"))
	{
		qsort(aPtr, counter, sizeof(const char*), compare);
		for (int i=0; i < counter; i++)
			printf("%s\t",aPtr[i]);
	}
	// closes the stream pointed to by dirP, returns -1 if an error occurs
	int status = closedir(dirP);
	printf("\n");
	if (status != 0)
		printf("An error closing the directory stream (in ls) has occured\n");

	return 0;
}

bool lsText(char text[])
{
	if (textEqual(text, "showFiles") || textEqual(text, "showfiles"))
		return true;
	else if (textEqual(text, "listDir") || textEqual(text, "listdir"))
		return true;
	else if (textEqual(text, "ls"))
		return true;
	return false;
}