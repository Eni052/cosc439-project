/*
// COSC 439 Operating Systems.
// Final Project.
// YOUR NAME: Olaniyan Eni, Idowu Adeseun, Olencz Kevin.
// PROGRAM-NAME: LINUX SHELL.
*/

#include "commands.h"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"


// the move command moves a file from one directory to another
// we have implemented it using the rename function from stdio.h
// rename will move a file only if the source and destination directories are part of the 
// same file system and partition
// Errors can be thrown by the function for several different reasons:
// trying to rename a file a directory name, write permission is missing, file doesn't exist,
// no room in the directory you are sending it to,
// if the file systems are different it will throw a EXDEV error, which could be caught here
// but the help page will notify you of this problem

// I have also included rename in this file because normally rename is a mv in bash

int move(struct Command cmd)
{
    // The rename function will move a file if given a new directory in the same file system
    if (cmd.numArgs <= 2)
        printf("Too few arguments presented to move, see help\n");
    if (cmd.numArgs == 3)
    {
        char dest[3000];
        strcpy(dest, cmd.args[2]);
        char slash[3] = "/";
        strcat(dest, slash);
        strcat(dest, cmd.args[1]);
        if(rename(cmd.args[1], dest) == -1)
        {
            printf(RED "**\nmove didn't work properly\n**\n" NORMAL);
        }
        else
        {
            printf("Successfully moved %s to %s\n", cmd.args[1], dest);
        }
    }
    if (cmd.numArgs > 3)
    {
        // absolute path
        if (textEqual(cmd.args[3], "ab"))
        {
            if (rename(cmd.args[1], cmd.args[2]) == -1)
            {
                printf(RED "**\nmove didn't work properly\n**\n" NORMAL);
            }
            else
            {
                printf("Successfully moved %s to %s\n", cmd.args[1], cmd.args[2]);
            }
        }
        else
        {
            printf("Invalid argument(s) presented to move: %s\n", cmd.args[3]);
        }
    }
    return 0;
}

int rname(struct Command cmd)
{
    if (cmd.numArgs != 3)
    {
        printf("Wrong number of arguments presented to rename. See help.\n");
        return 0;
    }

    if (rename(cmd.args[1], cmd.args[2]) == -1)
    {
        printf(RED "Rename didn't work properly\nSee help for help." NORMAL);
    }
    else
    {
        printf("Successfully renamed %s to %s\n", cmd.args[1], cmd.args[2]);
    }
    return 0;
}


// textEqual(command.args[0],"move")


bool moveText(char text[])
{
    if (textEqual(text, "mv") || textEqual(text, "move"))
        return true;
    else if (textEqual(text, "MV") || textEqual(text, "MV"))
        return true;
    return false;
}

bool rnameText(char text[])
{
    if (textEqual(text, "rename") || textEqual(text, "Rename"))
        return true;
    else if (textEqual(text, "rn") || textEqual(text, "RN"))
        return true;
    return false;
}