/*
// COSC 439 Operating Systems.
// Final Project.
// YOUR NAME: Olaniyan Eni, Idowu Adeseun, Olencz Kevin.
// PROGRAM-NAME: LINUX SHELL.
*/
#include "commands.h"

// prints current dir left of cursor

void printDirectory()
{
    char directory[1000];
    getcwd(directory, sizeof(directory));
    // check for bad copy from buffer to dir
    if (directory == NULL)
    	printf(RED "There is an ERROR stopping this from printing> " NORMAL);
    else
    	printf(CYAN "%s> " NORMAL, directory);
}