/*
// COSC 439 Operating Systems.
// Final Project.
// YOUR NAME: Olaniyan Eni, Idowu Adeseun, Olencz Kevin.
// PROGRAM-NAME: LINUX SHELL.
*/

#include "commands.h"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"

// run attempts to start a given file as it's own process
// run makes calls to dirent.h to make sure the file exists
// dirent.h includes methods to open/close/traverse directory streams

int runExternal(struct Command cmd)
{
	if (cmd.numArgs < 2)
	{
		printf("%s requires an argument. See help for help.\n", cmd.args[0]);
		return 0;
	}
	// program status for directory
	int status = 0;
	// program status for child created
	int childStatus;
	// flag for if it is a full path or not
	bool fullPath = false;
	// flag for whether the file exists or not
	bool exists = false;
	// A structure that holds basic info about a directory, including the name (import for ls)
	struct dirent *d;
	// this is a pointer to a directory object
	DIR *dirP;
	// To hold the current directory to view
	char* dir;
	// holds the file to execute
	char* ex;
	// if arguments are given, use second argument as path
	if (cmd.numArgs == 2)
	{
		dir = ".";
		// fullPath false by default
	}
	else if (cmd.numArgs == 3)
	{
		dir = cmd.args[1];
		ex = cmd.args[2];
		fullPath = true;
	}
	else if (cmd.numArgs > 3)
	{
		printf(RED"Too many arguments presented to run, see help\n"NORMAL);
		return 0;
	}
	// opens a directory "stream" to the directory passed by the argument
	// if successful, returns a pointer to a directory object
	dirP = opendir(dir);
	// if the directory was not successfully "opened"
	if (dirP == NULL)
	{
		printf(RED "An error occured trying to find this directory\n" NORMAL);
		if (cmd.numArgs > 1)
			printf(YELLOW "Please make sure the file or directory: %s exists\n" NORMAL, cmd.args[1]);
		return 0;
	}
	// this while loop will traverse the stream of objects in the directory
	// d will point to the next structure in the stream everytime readdir(directory*) is called
	while((d = readdir(dirP)) && !exists)
	{
		// access the current structure and print its name member
		// d_name is the name
		// d_type is the type of file (not supported on all platforms):
		// type 4 is a directory and type 8 is file
		// (you can find this by printing d_type)

		// for run we are looking for files, not directories
		if (d->d_type > 4)
		{
			// if the current files name matches the file to be run, the file exists
			if (textEqual(d->d_name, cmd.args[1]))
				exists = true;
		}
	}

	if (exists)
	{
		// create child
		pid_t pid = fork();
		// if child
		if (pid == 0)
	    {
	    	if (!fullPath)
	    	{
	    		char slash[1000] = "./";
	    		strcat(slash, cmd.args[1]);
	        	status = execl(slash, slash, NULL);
	        	_exit(status);
	    	}
	        else // fullPath // not working
	        {
	        	char location[5000];
				strcpy(location, dir);
				strcat(location, "/");
				strcat(location, ex);
	        	status = execl(location, location, NULL);
	        	_exit(status);
	        }
	    }
	    wait(NULL);
	} 

	// closes the stream pointed to by dirP, returns -1 if an error occurs
	status = closedir(dirP);
	printf("\n");
	if (status != 0)
		printf("An error closing the directory stream (in runExternal) has occured\n");

	return 0;
}

bool runText(char text[])
{
	if (textEqual(text, "run") || textEqual(text, "Run"))
		return true;
	else if (textEqual(text, "start") || textEqual(text, "Start"))
		return true;
	else if (textEqual(text, "execute") || (textEqual(text, "Execute")))
		return true;
	return false;
}