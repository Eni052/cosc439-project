/*
// COSC 439 Operating Systems.
// Final Project.
// YOUR NAME: Olaniyan Eni, Idowu Adeseun, Olencz Kevin.
// PROGRAM-NAME: LINUX SHELL.
*/
#include "commands.h"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"

void quit()
{
	printf("\nBye!\n\n");
    exit(0);
}
//////////////////////////////////////////////////////////////////////
bool quitText(char text[])
{
	if (textEqual(text, "quit") || textEqual(text, "QUIT"))
		return true;
	else if (textEqual(text, "EXIT") || textEqual(text, "exit"))
		return true;
	else if (textEqual(text, "Exit") || textEqual(text, "Quit"))
		return true;
	return false;
}