/*
// COSC 439 Operating Systems.
// Final Project.
// YOUR NAME: Olaniyan Eni, Idowu Adeseun, Olencz Kevin.
// PROGRAM-NAME: LINUX SHELL.
*/

#include "commands.h"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"

// mkdir creates a new directory (folder)
// mkdir is part of sys/stat.h

int makeDir(struct Command cmd)
{
	// check for invalid commands
	if (cmd.numArgs > 3)
	{
		printf("Too many arguments presented to mkdir");
		return 0;
	}
	else if (cmd.numArgs == 3 && !textEqual(cmd.args[2],"ab"))
	{
		printf("Invalid argument to %s: %s, see help %s for help", cmd.args[0],cmd.args[2],cmd.args[0]);
		return 0;
	}
	// to hold the directory we are going to paste together
	char directory[1000];
	// grab the path provided
	if (cmd.numArgs < 2)
	{
		printf(RED "You must provide a name for the directory you want to make\n" NORMAL);
		return 0;
	}
	const char* arg = cmd.args[1];
	// see pwd.c for documentation on getcwd(),
	// copies the current directory into directory
	if (cmd.numArgs <= 2) // create in current directory
	{
    	getcwd(directory, sizeof(directory));
    	if (directory == NULL)
	    {
	    	printf(RED "An error occured trying to get current directory" NORMAL);
	    	return 0;
	    }
	    // we will need to add the slash onto the arguments
	    char slash[1000] = "/";
	    // concatenate the slash onto the front of the argument
	    // strcat(destination, source)
	    strcat(slash, arg);
	    // again, but with the /arg to make dir/arg
		strcat(directory, slash);
	} // absolute path argument passed
	else if (cmd.numArgs == 3 && textEqual(cmd.args[2],"ab"))
	{
		strcpy(directory,cmd.args[1]);
	}

	// print feedback on what is being made
	printf("creating: %s", directory);
	// creates the char[] directory with the following flags:
	// S_IRWXU mask for file owner permissions
	// S_IRWXG mask for group permissions
	// S_IROTH others have read permission
	// S_IXOTH others have write permisison
	// returns -1 if an error occured and a directory will not be made
	int status = mkdir(directory, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	printf("\n");
	if (status != 0)
	{
		printf(RED "An error has occured making directory: %s\n" NORMAL, cmd.args[1]);
		printf(YELLOW "Make sure you aren't trying to create an existing directory\n" NORMAL);
		printf(YELLOW "Make sure you aren't trying to access protected directories '/' '/home'\n" NORMAL);
		printf(GREEN "See help for command help.\n" NORMAL);
	}
	else
		printf("Success!\n");
	return 0;
}

bool mkdirText(char text[])
{
	if (textEqual(text, "mkdir") || textEqual(text, "mkDir"))
		return true;
	else if (textEqual(text, "makeDir") || textEqual(text, "makedir"))
		return true;
	else if (textEqual(text, "makeDirectory") || textEqual(text, "makedirectory"))
		return true;
	return false;
}