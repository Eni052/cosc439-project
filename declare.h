#ifndef DECLARE_H
#define DECLARE_H

// declarations for functions in terminal.c
int terminalLoop(); // main loop
void printWelcome(); // prints welcome message
int run(struct Command); // runs a given command
void tokenize(struct Command*); // breaks up a line of input into tokens (command, then args)
void initArray(struct Command);


bool textEqual(char text[], char text2[]);



// declare the functions for your command here, to be used in other c files
void quit(); // exits the program
bool quitText(char text[]);

void help(struct Command); // prints a help message based on the passed command

int ls(struct Command); // prints all files and directories to the screen
bool lsText(char text[]);

int makeDir(struct Command); // makes a directory
bool mkdirText(char text[]);

void printDirectory(); // prints directory to the left of cursor

int move(struct Command);//moves a file
bool moveText(char text[]);

int rname(struct Command);//moves a file
bool rnameText(char text[]);

int changeDir(struct Command); // changes current directory
bool cdText(char text[]);

int makeFile(struct Command);

int rmv(struct Command);
bool rmText(char text[]);

bool runText(char text[]);
int runExternal(struct Command);


#endif
