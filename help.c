#include "commands.h"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"


void helpBasic()
{
	printf(YELLOW "\nSLS help\n\n" NORMAL);
	printf(GREEN "The currently implemented commands are:\n\n" NORMAL);
    printf(BLUE "quit/exit, showFiles/listDir/ls, mkdir/makedirectory, help\n" NORMAL);
    printf(BLUE "changeDirectory/cd, makeFile, move/mv, rename/rn,\n" NORMAL);
    printf(BLUE "remove/rm, run\n" NORMAL);
    printf(YELLOW "\nYou can type a command after help to learn more about how to use it\n\n" NORMAL);
}

void lsHelp(struct Command cmd)
{
	printf(YELLOW "\n%s help\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command shows you all the files in a given directory.\n" NORMAL);
	printf(GREEN "Providing no arguments lists the current directory.\n" NORMAL);
	printf(GREEN "%s 'DIRECTORY_NAME' will show you files in DIRECTORY_NAME.\n" NORMAL, cmd.args[1]);
	printf(GREEN "%s 'DIRECTORY_NAME' 'sort' will organize that directory alphabetically.\n" NORMAL, cmd.args[1]);
	printf("\n");
}

void mkdirHelp(struct Command cmd)
{
	printf(YELLOW "\n%s help\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command creates a directory for you.\n" NORMAL);
	printf(GREEN "%s 'DIRECTORY_NAME' will create DIRECTORY_NAME in the current directory.\n" NORMAL, cmd.args[1]);
	printf(GREEN "%s 'PATHTO/DIRECTORY_NAME' 'ab' will create the absolute path you give it.\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command will not give you permission to create directories in protected locations:\n" NORMAL);
	printf(GREEN "Such as root '/' and home '/home' \n" NORMAL);
	printf("\n");
}

void cdHelp(struct Command cmd)
{
	printf(YELLOW "\n%s help\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command changes the directory you are currently in.\n" NORMAL);
	printf(GREEN "%s 'DIRECTORY_NAME' will change your current directory.\n" NORMAL, cmd.args[1]);
	printf(GREEN "'DIRECTORY_NAME' can be an absolute bath.\n" NORMAL);
	printf("\n");
}

void mkfileHelp(struct Command cmd)
{
	printf(YELLOW "\n%s help\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command creates a file for you.\n" NORMAL);
	printf(GREEN "%s 'FILE_NAME' will create FILE_NAME in the current directory.\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command will not give you permission to create directories in protected locations:\n" NORMAL);
	printf(GREEN "Such as root '/' and home '/home' \n" NORMAL);
	printf("\n");
}

void mvHelp(struct Command cmd)
{
	printf(YELLOW "\n%s help\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command moves a file for you.\n" NORMAL);
	printf(GREEN "%s 'FILE_NAME' 'DIRECTORY_NAME' will move a file in your current directory\n" NORMAL, cmd.args[1]);
	printf(GREEN "%s 'FILE NAME' 'PATHTO/DIRECTORY_NAME' 'ab' will move a file to the directory in the given path\n" NORMAL, cmd.args[1]);
	printf(GREEN "If you are trying to rename a file, use the rename command or rename the file when giving the absolute path\n" NORMAL);
	printf(GREEN "This command will fail if used across two different file systems.\n" NORMAL);
	printf("\n");
}

void rnHelp(struct Command cmd)
{
	printf(YELLOW "\n%s help\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command renames a file or directory in your current directory.\n" NORMAL);
	printf(GREEN "%s 'EXISTING_NAME' 'NEW_NAME' will create rename in the current directory.\n" NORMAL, cmd.args[1]);
	printf("\n");
}

void pwdHelp(struct Command cmd)
{
	printf(YELLOW "\n%s help\n" NORMAL, cmd.args[1]);
	printf(GREEN "This is not a command. Your current directory is printed before every command\n" NORMAL);
	printf(GREEN "Piping is not implemented so you can't pipe the current directory into anything.\n\n" NORMAL);
}

void rmHelp(struct Command cmd)
{
	printf(YELLOW "\n%s help\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command moves removes a file for you.\n" NORMAL);
	printf(GREEN "%s 'FILE_NAME' will remove FILE_NAME from the current directory\n" NORMAL, cmd.args[1]);
	printf(GREEN "FILE_NAME can also be a path.\n" NORMAL);
	printf("\n");
}

void runHelp(struct Command cmd)
{
	printf(YELLOW "\n%s help\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command executes a file for you\n" NORMAL);
	printf(GREEN "It's use is intended for running compiled files.\n" NORMAL);
	printf(GREEN "In bash, this command is './FILE_NAME'\n" NORMAL);
	printf(GREEN "%s 'FILE_NAME' will execute a file in your current directory\n" NORMAL, cmd.args[1]);
	printf("\n");
}

void quitHelp(struct Command cmd)
{
	printf(YELLOW "\n%s help\n" NORMAL, cmd.args[1]);
	printf(GREEN "This command exits the shell for you\n" NORMAL);
	printf("\n");
}

void help(struct Command cmd)
{
	if (cmd.numArgs <= 1)
		helpBasic();
	else
	{
		if (lsText(cmd.args[1]))
			lsHelp(cmd);
		else if (cdText(cmd.args[1]))
			cdHelp(cmd);
		else if (mkdirText(cmd.args[1]))
			mkdirHelp(cmd);
		else if (moveText(cmd.args[1]))
			mvHelp(cmd);
		else if (rnameText(cmd.args[1]))
			rnHelp(cmd);
		else if (quitText(cmd.args[1]))
			quitHelp(cmd);
		else if (runText(cmd.args[1]))
			runHelp(cmd);
		else if (rmText(cmd.args[1]))
			rmHelp(cmd);
		else if (textEqual(cmd.args[1], "pwd"))
			pwdHelp(cmd);
		else if (textEqual(cmd.args[1], "makeFile"))
			mkfileHelp(cmd);
		else
			printf(RED "%s does not have a help page\n" NORMAL,cmd.args[1]);
	}
}