/*
// COSC 439 Operating Systems.
// Final Project.
// YOUR NAME: Olaniyan Eni, Idowu Adeseun, Olencz Kevin.
// PROGRAM-NAME: LINUX SHELL.
*/
#include <stdio.h>
#include <stdlib.h>
#include "commands.h"
#include <errno.h>
#include <fcntl.h>

int makeFile(struct Command cmd){

    FILE *fp;

    fp = fopen(cmd.args[1], "w");

    if(fp == NULL){

    printf("\nMake File Error on create\n");

    }else{

    fclose(fp);

    printf("\nMake File created successfully\n");

    return 0;
    }
}
