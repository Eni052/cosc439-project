/*
// COSC 439 Operating Systems.
// Final Project.
// YOUR NAME: Olaniyan Eni, Idowu Adeseun, Olencz Kevin.
// PROGRAM-NAME: LINUX SHELL.
*/
#include "commands.h"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"

int rmv(struct Command cmd)
{
   FILE *fpointer;
   fpointer = fopen(cmd.args[1], "w");
   if( fpointer != NULL)
   {
	unlink(cmd.args[1]);
        printf("file: %s is removed", cmd.args[1]);
   }
   else
   {
        printf("File Not Found");
   }
   fclose(fpointer);
}

//////////////////////////////////////////////////////////

bool rmText(char text[])
{
  if (textEqual(text, "remove") || textEqual(text, "removeFiles"))
    return true;
  else if (textEqual(text, "Remove") || textEqual(text, "rm"))
    return true;
  else if (textEqual(text, "RemoveFiles") || textEqual(text, "removefiles"))
    return true;
  return false;
}







