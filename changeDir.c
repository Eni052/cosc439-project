/*
// COSC 439 Operating Systems.
// Final Project.
// YOUR NAME: Olaniyan Eni, Idowu Adeseun, Olencz Kevin.
// PROGRAM-NAME: LINUX SHELL.
*/

#include "commands.h"
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"

int changeDir(struct Command cmd){
	char cwd[1000];

	printf("%s\n",getcwd(cwd,1000));

	chdir(cmd.args[1]);

	printf("%s\n",getcwd(cwd,1000));

	return 0;
}

bool cdText(char text[])
{
	if (textEqual(text, "changeDir") || textEqual(text, "changedir"))
		return true;
	else if (textEqual(text, "cd") || textEqual(text, "CD"))
		return true;
	else if (textEqual(text, "changeDirectory") || textEqual(text, "changedirectory"))
		return true;
	return false;
}