#ifndef COMMANDS_H
#define COMMANDS_H

#include "colors.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <errno.h>

struct Command {

	char line[1000]; // holds the input from the terminal
	char* args[100]; // tokenized version of the input, args[0] is the command, followed by 99 arguments
	int numArgs; // holds the number of current arguments, including the command
	int lineLength; // to remember the length
	int argLength; // to remember the array length

} command;

#endif